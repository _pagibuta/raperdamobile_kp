package com.example.raperdagson;

import java.util.List;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public interface RaperdaService {
    @GET("dokumen")
            Call<RaperdaAnswersResponse> getAnswers(
                    @QueryMap Map<String, String > paramater
    );

    @GET("/dokumen?search=peraturan&page=1")
        Call<List<RaperdaAnswersResponse>> getAnswers(@Query("tagged") String tags);
}
