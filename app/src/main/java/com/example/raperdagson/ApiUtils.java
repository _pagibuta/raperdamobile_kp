package com.example.raperdagson;

public class ApiUtils {
    public static final String BASE_URL = "https://jdih.bandung.go.id/api/";

    public static RaperdaService getRaperdaService (){
        return RetrofitClient.getClient(BASE_URL).create(RaperdaService.class);
    }
}
