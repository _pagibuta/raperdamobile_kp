package com.example.raperdagson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("nomor")
    @Expose
    private String nomor;
    @SerializedName("tahun")
    @Expose
    private String tahun;
    @SerializedName("perihal")
    @Expose
    private String perihal;
    @SerializedName("bentuk")
    @Expose
    private String bentuk;
    @SerializedName("bentuk_singkatan")
    @Expose
    private String bentukSingkatan;
    @SerializedName("lampiran_nama")
    @Expose
    private String lampiranNama;
    @SerializedName("lampiran_url")
    @Expose
    private String lampiranUrl;
    @SerializedName("detail_url")
    @Expose
    private String detailUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }

    public String getPerihal() {
        return perihal;
    }

    public void setPerihal(String perihal) {
        this.perihal = perihal;
    }

    public String getBentuk() {
        return bentuk;
    }

    public void setBentuk(String bentuk) {
        this.bentuk = bentuk;
    }

    public String getBentukSingkatan() {
        return bentukSingkatan;
    }

    public void setBentukSingkatan(String bentukSingkatan) {
        this.bentukSingkatan = bentukSingkatan;
    }

    public String getLampiranNama() {
        return lampiranNama;
    }

    public void setLampiranNama(String lampiranNama) {
        this.lampiranNama = lampiranNama;
    }

    public String getLampiranUrl() {
        return lampiranUrl;
    }

    public void setLampiranUrl(String lampiranUrl) {
        this.lampiranUrl = lampiranUrl;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

}

