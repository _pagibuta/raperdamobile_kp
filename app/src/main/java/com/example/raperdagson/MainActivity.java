package com.example.raperdagson;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private AnswersAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private RaperdaService mService;
    private EditText editText;
    String content = "";
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mService = ApiUtils.getRaperdaService();
        mRecyclerView = findViewById(R.id.rv_category);
        progressBar = findViewById(R.id.progressbar);
        editText = findViewById(R.id.KolomCari);
        mAdapter = new AnswersAdapter(this, new ArrayList<Datum>(0), new AnswersAdapter.PostItemListener(){

            @Override
            public void onPostClick (String id){
//                Toast.makeText(MainActivity.this, "Anda Memilih " + id, Toast.LENGTH_SHORT).show();
                String url = id;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        mRecyclerView.addItemDecoration(itemDecoration);


        showProgressBar(false);
        loadAnswers();


        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

    }



    private void loadAnswers() {
        showProgressBar(true);
        Map<String, String> parameter = new HashMap<>();
        parameter.put("search", content);
//        parameter.put("page", "1");
        mService.getAnswers(parameter).enqueue(new Callback<RaperdaAnswersResponse>() {
            @Override
            public void onResponse(Call<RaperdaAnswersResponse> call, Response<RaperdaAnswersResponse> response) {
                if(response.isSuccessful()){
                    mAdapter.updateAnswers(response.body().getData());
                    Log.d("MainActivity", "Loaded");
                }else {
                    int StatusCode = response.code();
                }
                List<Datum> items = response.body().getData();

                showProgressBar(false);
            }


            @Override
            public void onFailure(Call<RaperdaAnswersResponse> call, Throwable t) {
            showProgressBar(false);
            }
        });
    }

    private void performSearch(){
        content = editText.getText().toString();
//        Toast.makeText(MainActivity.this, content, Toast.LENGTH_SHORT).show();
        editText.clearFocus();
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(progressBar.getWindowToken(), 0);
        loadAnswers();

    }

    private void showProgressBar(boolean show){
        if(show){

            progressBar.setVisibility(View.VISIBLE);

        }else{
            progressBar.setVisibility(View.GONE);

        }
    }
}
