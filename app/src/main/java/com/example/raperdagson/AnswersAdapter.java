package com.example.raperdagson;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

public class AnswersAdapter extends RecyclerView.Adapter<AnswersAdapter.ViewHolder> {

    private List<Datum> mdatum;
    private Context mContext;
    private PostItemListener mPostItemListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView titleTv, nomorTV, tahunTV, bentukTV;
        PostItemListener mItemListener;

        public ViewHolder(View itemView, PostItemListener postItemListener) {
            super(itemView);
            titleTv = (TextView) itemView.findViewById(R.id.card_perihal);
            nomorTV = (TextView) itemView.findViewById(R.id.card_nomor);
            tahunTV = (TextView) itemView.findViewById(R.id.card_tahun);
            bentukTV = (TextView) itemView.findViewById(R.id.card_bentuk);

            this.mItemListener = postItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Datum datum = getDatum(getAdapterPosition());
            this.mItemListener.onPostClick(datum.getDetailUrl());



            notifyDataSetChanged();
        }


    }

    public AnswersAdapter(Context context, List<Datum> posts, PostItemListener itemListener) {
        mdatum = posts;
        mContext = context;
        mPostItemListener = itemListener;
    }

    @NonNull
    @Override
    public AnswersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View postView = inflater.inflate(R.layout.item_row_raperda, parent, false);
        ViewHolder viewHolder = new ViewHolder(postView, this.mPostItemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AnswersAdapter.ViewHolder holder, int position) {
        Datum datum = mdatum.get(position);
        TextView textView = holder.titleTv;
        textView.setText(datum.getPerihal());
        TextView textNomor = holder.nomorTV;
        textNomor.setText("Nomor: "+ datum.getNomor());
        TextView textTahun = holder.tahunTV;
        textTahun.setText("Tahun: "+ datum.getTahun());
        TextView textBentuk = holder.bentukTV;
        textBentuk.setText("Bentuk: " +datum.getBentuk());
    }

    @Override
    public int getItemCount() {
        return mdatum.size();
    }

    public void updateAnswers(List<Datum> datums) {
        mdatum = datums;
        notifyDataSetChanged();

    }

    private Datum getDatum(int adapterPosition) {
        return mdatum.get(adapterPosition);
    }

    public interface PostItemListener {
        void onPostClick(String id);
    }
}
